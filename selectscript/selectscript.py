from math import pow

def py_add(lhs, rhs):
    return lhs + rhs

def py_sub(lhs, rhs):
    return lhs - rhs

def py_mul(lhs, rhs):
    return lhs * rhs

def py_div(lhs, rhs):
    return lhs / rhs

def py_mod(lhs, rhs):
    return lhs % rhs

def py_pow(lhs, rhs):
    return pow(lhs, rhs)

def py_eq(lhs, rhs):
    if lhs == rhs:
        return 1
    else:
        return 0

def py_ne(lhs, rhs):
    if lhs != rhs:
        return 1
    else:
        return 0

def py_ge(lhs, rhs):
    if lhs >= rhs:
        return 1
    else:
        return 0

def py_gt(lhs, rhs):
    if lhs > rhs:
        return 1
    else:
        return 0

def py_le(lhs, rhs):
    if lhs <= rhs:
        return 1
    else:
        return 0

def py_lt(lhs, rhs):
    if lhs < rhs:
        return 1
    else:
        return 0
