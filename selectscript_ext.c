#include <Python.h>
#include <selectscript.h>
#include <result.h>
#include <container.h>
#include <deque.h>

PyObject *module = NULL;

struct ss_state *state = NULL;

/* library functions */
void *lib_add(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_add");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }
}

void *lib_sub(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_sub");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }

    return NULL;
}

void *lib_mul(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_mul");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }

    return NULL;
}

void *lib_div(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_div");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }

    return NULL;
}

void *lib_mod(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_mod");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }

    return NULL;
}

void *lib_pow(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_pow");

    if (func && PyCallable_Check(func)) {
        return PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        Py_RETURN_NONE;
    }

    return NULL;
}

int lib_eq(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_eq");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

int lib_ne(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_ne");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

int lib_ge(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_ge");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

int lib_gt(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_gt");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

int lib_le(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_le");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

int lib_lt(void *lhs, void *rhs)
{
    PyObject *func = PyObject_GetAttrString(module, "py_lt");
    PyObject *result = NULL;

    if (func && PyCallable_Check(func)) {
        result = PyObject_CallFunctionObjArgs(func, lhs, rhs, NULL);
    } else {
        return 0;
    }

    return PyLong_AsLong(result);
}

/* SelectScript Python API */
static PyObject *selectscript_add_env(PyObject *self, PyObject *args)
{
    char *name = NULL;
    PyObject *o = NULL;
    PyArg_ParseTuple(args, "sO", &name, &o);

    if (PyList_Check(o)) {
        struct deque *list = NULL;

        for (int i = 0; i < PyList_Size(o); i++) {
            PyObject *item = PyList_GetItem(o, i);
            struct container *c = calloc(1, sizeof(struct container));
            c->type = C_EXTERNAL;
            c->ext = item;

            deque_push_last(&list, c);
        }

        add_environment_element(ENV_LIST, name, list);
    }

    Py_RETURN_NONE;
}

static PyObject *selectscript_new(PyObject *self, PyObject *args)
{
    char *script = NULL;
    PyArg_ParseTuple(args, "s", &script);
    state = ss_new(script);

    Py_RETURN_NONE;
}

static PyObject *selectscript_evaluate(PyObject *self)
{
    ss_evaluate(state);

    if (state->result) {
        PyObject *result = PyList_New(0);

        struct deque_iterator *di_global = deque_iterator_init(&(state->result));
        struct result *local = NULL;
        while ((local = deque_iterator_next(di_global)) != NULL) {
            struct deque_iterator *di_local = deque_iterator_init(&(local->data));

            if (local->type == R_LIST) {
                PyObject *list = PyList_New(0);

                struct container *c = NULL;
                while ((c = deque_iterator_next(di_local)) != NULL) {
                    switch (c->type) {
                        case C_STRING:
                            PyList_Append(list, PyString_FromString(c->strng));
                            break;
                        case C_FLOAT:
                            PyList_Append(list, PyFloat_FromDouble(c->flt));
                            break;
                        case C_INTEGER:
                            PyList_Append(list, PyLong_FromLong(c->ntgr));
                            break;
                        case C_BOOLEAN:
                            PyList_Append(list, PyBool_FromLong(c->bln));
                            break;
                        case C_EXTERNAL:
                            PyList_Append(list, c->ext);
                            break;
                    }
                }

                PyList_Append(result, list);
            } else {
                PyObject *dict = PyDict_New();

                struct hashmap_entry *entry = NULL;
                while ((entry = deque_iterator_next(di_local)) != NULL) {
                    struct container *c = entry->value;

                    switch (c->type) {
                        case C_STRING:
                            PyDict_SetItem(dict, PyString_FromString(entry->key), PyString_FromString(c->strng));
                            break;
                        case C_FLOAT:
                            PyDict_SetItem(dict, PyString_FromString(entry->key), PyFloat_FromDouble(c->flt));
                            break;
                        case C_INTEGER:
                            PyDict_SetItem(dict, PyString_FromString(entry->key), PyLong_FromLong(c->ntgr));
                            break;
                        case C_BOOLEAN:
                            PyDict_SetItem(dict, PyString_FromString(entry->key), PyBool_FromLong(c->bln));
                            break;
                        case C_EXTERNAL:
                            PyDict_SetItem(dict, PyString_FromString(entry->key), c->ext);
                            break;
                    }
                }

                PyList_Append(result, dict);
            }
        }

        return result;
    }

    Py_RETURN_NONE;
}

static PyObject *test(PyObject *self)
{
    PyObject *one = PyLong_FromLong(2);
    PyObject *two = PyLong_FromLong(1);

    fprintf(stderr, "RESULT: %d\n", lib_lt(one, two));

    Py_RETURN_NONE;
}

static PyMethodDef selectscript_ext_funcs[] = {
    { "test", (PyCFunction)test, METH_NOARGS, NULL },
    { "selectscript_add_env", (PyCFunction)selectscript_add_env, METH_VARARGS, NULL },
    { "selectscript_new", (PyCFunction)selectscript_new, METH_VARARGS, NULL },
    { "selectscript_evaluate", (PyCFunction)selectscript_evaluate, METH_NOARGS, NULL },
    { NULL }
};

void initselectscript_ext(void)
{
    /* fill SelectScript library */
    add_library_function(FID_ADD, lib_add);
    add_library_function(FID_SUB, lib_sub);
    add_library_function(FID_MUL, lib_mul);
    add_library_function(FID_DIV, lib_div);
    add_library_function(FID_MOD, lib_mod);
    add_library_function(FID_POW, lib_pow);
    add_library_function(FID_EQ, lib_eq);
    add_library_function(FID_NE, lib_ne);
    add_library_function(FID_GE, lib_ge);
    add_library_function(FID_GT, lib_gt);
    add_library_function(FID_LE, lib_le);
    add_library_function(FID_LT, lib_lt);

    /* initialize module */
    Py_InitModule3("selectscript_ext", selectscript_ext_funcs, "SelectScript Python Binding C extension");

    PyObject *name = PyString_FromString("selectscript");
    module = PyImport_Import(name);
    Py_DECREF(name);
}
