LIB = selectscript_ext.so

CFLAGS = -std=c11 -fPIC -shared -Wall -g -Werror -I../libselectscript/src -I../libselectscript/libcollect `pkg-config --cflags python`
LDFLAGS = -L../libselectscript -L../libselectscript/libcollect `pkg-config --libs-only-L python`
LDLIBS = -lselectscript -lcollect `pkg-config --libs-only-l python`

.PHONY: all clean

all: $(LIB)

%.so: %.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(LDLIBS)

clean:
	rm -f *.so
	rm -rf *.dSYM
	rm -f selectscript/*.pyc
