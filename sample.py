from selectscript import *
from time import time

t_main = time()

problem = """select
a.this, b.this, c.this, d.this,
e.this, f.this, g.this, h.this

from
a=colors, b=colors, c=colors, d=colors,
e=colors, f=colors, g=colors, h=colors

where
a.this != b.this and a.this != c.this and
a.this != d.this and a.this != e.this and
a.this != f.this and
b.this != c.this and b.this != d.this and
b.this != f.this and b.this != g.this and
c.this != d.this and c.this != g.this and
c.this != h.this and
d.this != e.this and d.this != h.this and
e.this != f.this and e.this != h.this and
f.this != g.this and g.this != h.this as dict;"""

colors = ['red', 'green', 'blue', 'yellow']
selectscript_add_env('colors', colors)

t_new = time()
selectscript_new(problem)
print "selectscript_new took", time() - t_new, "seconds."

t_evaluate = time()
result = selectscript_evaluate()
print "selectscript_evaluate took", time() - t_evaluate, "seconds."

print result

print "main took", time() - t_main, "seconds."
